icinga2-sublime
============  
_Icinga2 package for Sublime Text_

A small package that provides syntax highlighting, block commenting, and snippet support for
defining hosts/services etc.

INSTALLATION
-------------
<dl>
<dt>Manually:</dt>
<dd>In the Sublime preferences menu select 'Browse Packages,' that'll show the directory where your
packages are installed.

open a terminal, cd to the packages directory and then clone this project:

	git clone https://gitlab.com/prbsparx/icinga2-sublime.git
</dd>
<dt>Using Package Control:</dt>
<dd>Open the Command Pallete - press *'ctrl+shift+p'* (Windows, Linux) or *'cmd+shift+p'* (OS X).  
All Package Control commands begin with *'Package Control:'*, so view the list of
commands by typing *'package'*.

######Add Repository  
Package Control: Add Repository  

	https://gitlab.com/prbsparx/icinga2-sublime


######Install Package  
Package Control: Install Package  
type *'icinga2'* to shorten the list and select *'icinga2-sublime'*.
</dd>
REMOVE
------
<dt>Manually:</dt>
<dd>Delete directory from your packages directory.</dd>
<dt>Using Package Control:</dt>
<dd>Package Control: Remove Package  
select *'icinga2-sublime'*
</dd>

CREDIT
-------------
Copied and modified from https://github.com/bn0/nagios-sublime by @bn0